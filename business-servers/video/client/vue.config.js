module.exports = {
  devServer: {
    proxy: {
      '/api/resource': {
        target: 'http://localhost:3000',
        changeOrigin: true
      }
    }
  },
  configureWebpack: config => {
    config.output.filename = 'video.[name].js'
  },
  assetsDir: "video-static",
  baseUrl: "video/"
}
