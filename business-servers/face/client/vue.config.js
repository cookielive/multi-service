module.exports = {
  devServer: {
    proxy: {
      '/api/resource': {
        target: 'http://localhost:3000',
        changeOrigin: true
      }
    }
  },
  configureWebpack: config => {
    config.output.filename = 'face.[name].js'
  },
  assetsDir: "face-static",
  baseUrl: "face/"
}
