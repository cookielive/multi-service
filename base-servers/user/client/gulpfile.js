const gulp = require('gulp')

gulp.task('copystatic', function(){
  return gulp.src(["assets/*.js", "assets/*.css"])
    .pipe(gulp.dest('dist/static'))
})

gulp.task('copyfile', function(){
  return gulp.src("./*.html")
    .pipe(gulp.dest('dist'))
})

gulp.task('default', gulp.parallel('copystatic', 'copyfile'))