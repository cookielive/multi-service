// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

Vue.config.productionTip = false
Vue.prototype.$axios = axios

Vue.use(ElementUI)
// axios 请求拦截器处理请求数据
// axios.interceptors.request.use(config => {
//   const token = localStorage.getItem('token')
//   config.headers.common['Authorization'] = 'Bearer ' + token
//   return config
// })

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
