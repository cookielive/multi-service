const execFile = require("child_process").execFile;
execFile(
  "node",
  ["unpack.js"],
  { cwd: "./public/main/dist/js/" },
  (error, stdout, stderr) => {
    if (error) {
      throw error;
    }
    console.log(stdout);
  }
);
module.exports = {
  apps: [
    // 主项目
    {
      name: "SchoolPlantForm",
      script: "./bin/www",
      // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
      instances: 1,
      autorestart: true,
      max_memory_restart: "1G"
      // 监控变化的目录，一旦变化，自动重启
      // watch: "./projects"
      // env: {
      //   NODE_ENV: "development"
      // },
      // env_production: {
      //   NODE_ENV: "production"
      // }
    }
  ]

  // deploy: {
  //   production: {
  //     user: "node",
  //     host: "212.83.163.1",
  //     ref: "origin/master",
  //     repo: "git@github.com:repo.git",
  //     path: "/var/www/production",
  //     "post-deploy":
  //       "npm install && pm2 reload ecosystem.config.js --env production"
  //   }
  // }
};
